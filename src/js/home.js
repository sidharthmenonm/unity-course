import Vue from 'vue';
import csvtojson from './csvtojson';

new Vue({
  el: '#webinar',
  data: {
    data: [],
    key: '2PACX-1vS9LqfIkZC3gdcfd5KmQ_PxOKMNF_ZJ5mXxhu_UYUYJWaiGT-zKtzHJef2tFcb2g0HpmSeeGuWDbp3m',
    gid: '1793042154'
  },
  computed: {
    speakers: function() {
      return this.sort('speaker')
    },
    investors: function() {
      return this.sort('curator')
    }
  },
  methods: {
    sort: function(typ) {
      var sorted = this.data.filter(function(item) {
        return item.type == typ;
      })

      // Set slice() to avoid to generate an infinite loop!
      sorted = sorted.slice().sort(function(a, b) {
        return a.order - b.order;
      });

      return sorted.filter(function(item) {
        return item.order > 0
      })
    },
    loadData: function() {
      var vm = this;
      fetch(`https://docs.google.com/spreadsheets/d/e/${this.key}/pub?gid=${this.gid}&single=true&output=csv`)
        .then(response => response.text())
        .then(csv => csvtojson(csv))
        .then(function(json) {
          vm.data = JSON.parse(json);
        });
    }

  },
  mounted() {
    this.loadData();
    jQuery('.video-pop-up').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false
    });
  }
})