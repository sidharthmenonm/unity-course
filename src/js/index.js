import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";
import SmoothScroll from 'smooth-scroll';
import magnificPopup from 'magnific-popup';

window.$ = window.jQuery = jQuery;
window.bootstrap = bootstrap;
window.popper = popper;

// $('.video-pop-up').magnificPopup({
//   type: 'iframe',
//   mainClass: 'mfp-fade',
//   removalDelay: 160,
//   preloader: false,
//   fixedContentPos: false
// });

$.magnificPopup.open({
  items: {
    src: 'awareness.jpg'
  },
  type: 'image',
  closeOnContentClick: true,
  image: {
    markup: `
      <div class="mfp-figure">
        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
        <figure>
          <a href="#webinar">
            <div class="mfp-img mfp-close"></div>
          </a>
          <figcaption>
            <div class="mfp-bottom-bar">
              <div class="mfp-title"></div>
              <div class="mfp-counter"></div>
            </div>
          </figcaption>
        </figure>
      </div>
    `
  }
});

// $('.img-pop-up').magnificPopup({
//   type: 'image',
//   gallery: {
//       enabled: true
//   }
// });

$(window).on('scroll', function() {
  if ($(window).scrollTop() < 300) {
    $('.site-navigation').removeClass('sticky_header');
  } else {
    $('.site-navigation').addClass('sticky_header');
  }
});

var scroll = new SmoothScroll('a[data-scroll]');

$('.hamburger-menu').on('click', function() {
  $(this).toggleClass('open');
  $('.site-navigation').toggleClass('show');
});

var pathname = location.pathname.split("/");
console.log(pathname);
$('.site-navigation a[href="' + pathname[pathname.length - 1] + '"]').parent().addClass('current-menu-item');